export class CreateLoanPayload {
    personalId?: string;
    firstName?: string;
    lastName?: string;
    birthDate?: any;
    employer?: string;
    salary?: number;
    monthlyLiability?: number;
    requestedAmount?: number;
    requestedTerm?: string;
    status?: string;
}