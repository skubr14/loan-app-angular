import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { LoanService } from 'src/app/auth/shared/loan.service';
import { CreateLoanPayload } from 'src/app/loan-form/loan-form.payload';

@Component({
  selector: 'app-loan-form',
  templateUrl: './loan-form.component.html',
  styleUrls: ['./loan-form.component.css']
})

export class LoanFormComponent implements OnInit {

  createLoanForm: FormGroup;
  loanPayload: CreateLoanPayload;

  constructor(private router: Router, private loanService: LoanService,
    private _location: Location) {
    this.loanPayload = {
      personalId: '',
      firstName: '',
      lastName: '',
      employer: '',
      salary: 0,
      birthDate: new Date(),
      monthlyLiability: 0,
      requestedAmount: 0,
      requestedTerm: '',
    }

   }

  ngOnInit(): void {
    
    this.createLoanForm = new FormGroup({
      personalId: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      surname: new FormControl('', Validators.required),
      employer: new FormControl('', Validators.required),
      salary: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
      liability: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      term: new FormControl('', Validators.required),

    });
  }

  createLoan() {
    this.loanPayload.personalId = this.createLoanForm.get('personalId').value;
    this.loanPayload.firstName = this.createLoanForm.get('name').value;
    this.loanPayload.lastName = this.createLoanForm.get('surname').value;
    this.loanPayload.employer = this.createLoanForm.get('employer').value;
    this.loanPayload.salary = this.createLoanForm.get('salary').value;
    this.loanPayload.birthDate = new Date(this.createLoanForm.get('date').value);
    this.loanPayload.monthlyLiability = this.createLoanForm.get('liability').value;
    this.loanPayload.requestedAmount = this.createLoanForm.get('amount').value;
    this.loanPayload.requestedTerm = this.createLoanForm.get('term').value;
    this.loanService.createLoan(this.loanPayload).subscribe((data) => {
    this._location.back();
    }, error => {
      throwError(error);
    })
  }

}
