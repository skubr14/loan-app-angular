import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CreateLoanPayload } from '../../loan-form/loan-form.payload';
import { LoanModel } from './loan.model';

@Injectable({
  providedIn: 'root'
})
export class LoanService {

  constructor(private http: HttpClient) { }

  getAllLoans(): Observable<Array<LoanModel>> {
    return this.http.get<Array<LoanModel>>('http://localhost:8080/api/loan-application/');
  }

  createLoan(loanPayload: CreateLoanPayload): Observable<any> {
    return this.http.post('http://localhost:8080/api/loan-application/', loanPayload);
  }

  updateLoanStatus(status: string, id: number): Observable<any> {

    return this.http.post('http://localhost:8080/api/loan-application/edit/' + id, status);

  }

  deleteLoan(id: number) : Observable<any> {
    console.log(id)
    return this.http.delete('http://localhost:8080/api/loan-application/' + id);
  }

  //check routes

  getLoan(id: number): Observable<LoanModel> {
    return this.http.get<LoanModel>('http://localhost:8080/api/loan-application/' + id);
  }

}