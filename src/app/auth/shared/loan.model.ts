export class LoanModel {
    id: number;
    firstName: string;
    lastName: string;
    birthDate: Date;
    employer: string;
    salary: number;
    monthlyLiability: number;
    requestedAmount: number;
    requestedTerm: string;
    score: number;
    status: string;
}