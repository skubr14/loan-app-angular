import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoanService } from '../shared/loan.service';
import { LoanModel } from '../shared/loan.model';
import { CreateLoanPayload } from 'src/app/loan-form/loan-form.payload';
import { throwError } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {
  name: string;
  loans: LoanModel[];
  options:string[] =  ["surNameAsc", "surNameDesc"];
  editLoanPayload : CreateLoanPayload = {status:''};
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private loanService : LoanService) {
    this.name = this.activatedRoute.snapshot.params.name;
    this.loadLoans();
  }

  loadLoans() {
    this.loanService.getAllLoans().subscribe(data => {
      this.loans = data;
    });
  }

  addLoanApplication() {
    this.router.navigateByUrl('/create-loan');
  }

  approveLoanApplication(id: number){
    this.loanService.updateLoanStatus("Approved", id).subscribe((data) => {
      this.loadLoans();
    }, error => {
      throwError(error);
    })
  }

  rejectApplication(id: number){
    this.loanService.updateLoanStatus("Rejected", id).subscribe((data) => {
      this.loadLoans();
    }, error => {
      throwError(error);
    })
  }

  deleteLoanApplication(id: number){
    this.loanService.deleteLoan(id).subscribe((data) => {
      this.loadLoans();
    }, error => {
      throwError(error);
    })
  }

  getDate(dateMillis: number){
    return moment.unix(dateMillis).format("DD MMM YYYY")
  }

  ngOnInit(): void {

  }

}
